## Usage

module "flow_logs" {
  source = "github.com/GSA/terraform-vpc-flow-log"
  vpc_id = "${aws_vpc.main.id}"
}

